import 'package:flutter/material.dart';
import 'package:wall_candy/views/screens/category_screen.dart';
import 'package:wall_candy/views/screens/full_screen.dart';
import 'package:wall_candy/views/screens/home_screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wall_candy/views/screens/search_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        canvasColor: Colors.grey.shade100,
        primarySwatch: Colors.blueGrey,
        fontFamily: GoogleFonts.poppins().fontFamily,
        iconTheme: IconThemeData(
          color: Colors.blueGrey, 
        ),
      ),
      home: HomeScreen(),
      routes: {
        HomeScreen.routeName: (ctx) => HomeScreen(),
        SearchScreen.routeName: (ctx) => SearchScreen(query: 'pastel'),
        // CategoryScreen.routeName: (ctx) => CategoryScreen(),
        // FullScreen.routeName: (ctx) => FullScreen(),
      },
    );
  }
}
