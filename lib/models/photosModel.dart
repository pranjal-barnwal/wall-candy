class PhotosModel {
  String imgSrc;
  String photographer;
  String desc;

  PhotosModel({
    required this.imgSrc,
    required this.photographer,
    required this.desc,
  });

  static fromApiToApp(Map<String, dynamic> photoMap) {
    return PhotosModel(
      imgSrc: photoMap['src']['portrait'],
      photographer: photoMap['photographer'],
      desc: photoMap['alt'],
    );
  }
}
