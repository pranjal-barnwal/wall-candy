import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:wall_candy/models/photosModel.dart';

class ApiOperations {
  static List<PhotosModel> trendingWallpapers = [];
  static List<PhotosModel> searchWallpapers = [];

  static Future<List<PhotosModel>> getTrendingWallpapers() async {
    await http.get(
      Uri.parse('https://api.pexels.com/v1/curated?per_page=70'),
      headers: {"Authorization": "hgg5TJErEe2Km2Y1f3HcG1tcLoVWwXoGo885KFV2rMtVCiQmKhr2Ee5C"}
    ).then((response) {
      // print(response.body);
      Map<String,dynamic> jsonData = jsonDecode(response.body);
      List photos = jsonData['photos'];
      trendingWallpapers.clear();
      photos.forEach((photo) {
        // Map<String, dynamic> src = photo['src'];
        // print(src['portrait']);
        trendingWallpapers.add(PhotosModel.fromApiToApp(photo));
      });
      print(photos);
    });
    return trendingWallpapers;
  }

  static Future<List<PhotosModel>> getSearchedWallpapers(String query) async {
    await http.get(
      Uri.parse('https://api.pexels.com/v1/search?query=$query&per_page=70&page=1'),
      headers: {"Authorization": "hgg5TJErEe2Km2Y1f3HcG1tcLoVWwXoGo885KFV2rMtVCiQmKhr2Ee5C"}
    ).then((response) {
      // print(response.body);
      Map<String,dynamic> jsonData = jsonDecode(response.body);
      List photos = jsonData['photos'];
      searchWallpapers.clear();
      photos.forEach((photo) {
        // Map<String, dynamic> src = photo['src'];
        // print(src['portrait']);
        searchWallpapers.add(PhotosModel.fromApiToApp(photo));
      });
      print(photos);
    });
    return searchWallpapers;
  }  
  
}