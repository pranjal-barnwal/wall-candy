class CategorieModel {
  String categorieName;
  String imgUrl;
  CategorieModel({
    required this.categorieName,
    required this.imgUrl,
  });
}

List<CategorieModel> getCategories() {
  List<CategorieModel> categories = [];
  String name, url;

  name = "Aesthetic";
  url = "https://images.pexels.com/photos/6063551/pexels-photo-6063551.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
  categories.add(CategorieModel(categorieName: name, imgUrl: url));

  name = "Minimal";
  url = "https://images.pexels.com/photos/3297593/pexels-photo-3297593.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
  categories.add(CategorieModel(categorieName: name, imgUrl: url));

  name = "Abstract";
  url = "https://images.pexels.com/photos/1910225/pexels-photo-1910225.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
  categories.add(CategorieModel(categorieName: name, imgUrl: url));

  name = "Artistic";
  url = "https://images.pexels.com/photos/1616403/pexels-photo-1616403.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
  categories.add(CategorieModel(categorieName: name, imgUrl: url));

  name = "Forest";
  url = "https://images.pexels.com/photos/1770809/pexels-photo-1770809.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
  categories.add(CategorieModel(categorieName: name, imgUrl: url));

  name = "Sky";
  url = "https://images.pexels.com/photos/5245865/pexels-photo-5245865.jpeg?auto=compress&cs=tinysrgb&w=1600";
  categories.add(CategorieModel(categorieName: name, imgUrl: url));

  name = "Sea";
  url = "https://images.pexels.com/photos/1482193/pexels-photo-1482193.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
  categories.add(CategorieModel(categorieName: name, imgUrl: url));

  name = "India";
  url = "https://images.pexels.com/photos/7919683/pexels-photo-7919683.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
  categories.add(CategorieModel(categorieName: name, imgUrl: url));
  

  return categories;
}