import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomAppBar extends StatelessWidget {
  final String word1;
  final String word2;

  const CustomAppBar({
    Key? key,
    required this.word1,
    required this.word2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: word1,
              style: GoogleFonts.pacifico(
                fontSize: 28,
                fontWeight: FontWeight.w900,
                color: Colors.blueGrey.shade700,
                fontStyle: FontStyle.italic,
              ),
            ),
            TextSpan(
              text: word2,
              style: GoogleFonts.pacifico(
                fontSize: 28,
                fontWeight: FontWeight.w900,
                color: Colors.blue,
                fontStyle: FontStyle.italic,
              ),
            ),
          ],
          style: const TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
    );
  }
}
