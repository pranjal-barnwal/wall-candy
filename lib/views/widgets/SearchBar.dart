import 'package:flutter/material.dart';
import 'package:wall_candy/views/screens/search_screen.dart';

class SearchBar extends StatelessWidget {
  TextEditingController _searchController = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: Colors.white38,
        border: Border.all(
          color: Colors.blueGrey.shade100,
          width: 2,
        ),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: _searchController,
              decoration: const InputDecoration(
                hintText: 'Search here:',
                hintStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black38,
                ),
                border: InputBorder.none,
              ),
            ),
          ),
          InkWell(
            onTap: (){
              // print('SEARCHING...');
              // Navigator.of(context).pushNamed(SearchScreen(query: _searchController.text));
              Navigator.push(context, MaterialPageRoute(builder: (ctx)=> SearchScreen(query: _searchController.text)));
              // Navigator.pushReplacement(context, MaterialPageRoute(builder: (ctx)=> SearchScreen(query: _searchController.text)));
            },
            child: const Icon(Icons.search_outlined),
          ),
        ],
      ),
    );
  }
}
