import 'package:flutter/material.dart';

class CatBlock extends StatelessWidget {
  String catImgSrc;
  String category;
  CatBlock({super.key, required this.catImgSrc, required this.category});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 8),
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Hero(
              tag: catImgSrc,
              child: Image.network(
                catImgSrc,
                height: 60,
                width: 100,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            height: 60,
            width: 100,
            decoration: BoxDecoration(
              color: Colors.black54,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Center(
              child: Text(
                '$category',
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
