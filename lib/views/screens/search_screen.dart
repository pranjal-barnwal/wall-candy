import 'package:flutter/material.dart';
import 'package:wall_candy/views/widgets/CustomAppBar.dart';
import 'package:wall_candy/views/widgets/SearchBar.dart';
import 'package:wall_candy/views/screens/full_screen.dart';

import '../../controller/apiOperation.dart';
import '../../models/photosModel.dart';
import 'home_screen.dart';

class SearchScreen extends StatefulWidget {
  static const routeName = '/search';
  final String query;

  SearchScreen({super.key, required this.query});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  List<PhotosModel> searchWallpapers = [];

  GetSearchResults() async {
    searchWallpapers = await ApiOperations.getSearchedWallpapers(widget.query);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    GetSearchResults();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
          color: Colors.black54, // Change the color here
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor,
        title: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0, right: 55),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(
                  Icons.support,
                  color: Colors.black54,
                  size: 32,
                ),
                SizedBox(
                  width: 5,
                ),
                CustomAppBar(word1: 'Wall', word2: 'Candy'),
              ],
            ),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SearchBar(),
            const SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.grey,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal:10, vertical:5),
                child: Text(
                  'Results for: ${widget.query}',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            Expanded(
              child: SizedBox(
                child: GridView.builder(
                  physics: BouncingScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    mainAxisExtent: 300,
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                  ),
                  itemCount: searchWallpapers.length,
                  itemBuilder: (ctx, idx) => InkWell(
                    splashColor: Colors.blueGrey,
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (ctx) => FullScreen(
                            imgUrl: searchWallpapers[idx].imgSrc,
                            photographer: searchWallpapers[idx].photographer,
                            desc: searchWallpapers[idx].desc,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.grey,
                      ),
                      clipBehavior: Clip.hardEdge,
                      child: Hero(
                        tag: searchWallpapers[idx].imgSrc,
                        child: Image.network(
                          searchWallpapers[idx].imgSrc,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
