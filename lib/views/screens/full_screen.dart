import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:image_downloader/image_downloader.dart';

class FullScreen extends StatelessWidget {
  static const routeName = '/full';

  String imgUrl;
  String photographer;
  String desc;
  FullScreen({
    Key? key,
    required this.imgUrl,
    required this.photographer,
    required this.desc,
  }) : super(key: key);

  // Future<void> setwallpaper(BuildContext context) async {
  //      var imageId =
  //          await ImageDownloader.downloadImage("https://raw.githubusercontent.com/wiki/ko2ic/image_downloader/images/bigsize.jpg");
  //      if (imageId == null) {
  //         return;
  //      }
  //      var fileName = await ImageDownloader.findName(imageId);
  //      var path = await ImageDownloader.findPath(imageId);
  //      var size = await ImageDownloader.findByteSize(imageId);
  //      var mimeType = await ImageDownloader.findMimeType(imageId);
  //      Navigator.pop(context);
  //     }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Hero(
            tag: imgUrl,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(imgUrl),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 30,
            left: 50,
            child: SizedBox(
              width: MediaQuery.of(context).size.width - 100,
              height: 50,
              child: ElevatedButton.icon(
                icon: Icon(Icons.save_alt),
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.grey.shade900,
                  shape: StadiumBorder(),
                ),
                onPressed: () async {
                  var imageId = await ImageDownloader.downloadImage(imgUrl);
                  if (imageId == null) {
                    print('⚠️ERROR: Unknown exception while downloading image');
                  }
                },
                label: Text(
                  'Download',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: 20,
            left: 20,

            child: Ink(
              decoration: ShapeDecoration(
                color: Colors.grey.shade900,
                shape: CircleBorder(),
              ),
              child: IconButton(
                icon: Icon(Icons.chevron_left),
                style: IconButton.styleFrom(backgroundColor: Colors.redAccent),
                color: Colors.white,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     body: Container(
  //         child: Column(
  //       children: [
  //         Expanded(
  //           child: Container(
  //             child: Image.network(imgUrl),
  //           ),
  //         ),
  //         InkWell(
  //           onTap: () async {
  //             var imageId = await ImageDownloader.downloadImage(imgUrl);
  //             if (imageId == null) {
  //               print('⚠️ERROR: Unknown exception while downloading image');
  //             }
  //           },
  //           child: Container(
  //             height: 60,
  //             width: double.infinity,
  //             color: Colors.black,
  //             child: const Center(
  //               child: Text('Download Wallpaper',
  //                   style: TextStyle(fontSize: 20, color: Colors.white)),
  //             ),
  //           ),
  //         )
  //       ],
  //     )),
  //   );
  // }

}
