import 'package:flutter/material.dart';
import 'package:wall_candy/controller/apiOperation.dart';
import 'package:wall_candy/data.dart';
import 'package:wall_candy/models/photosModel.dart';
import 'package:wall_candy/views/screens/category_screen.dart';
import 'package:wall_candy/views/screens/full_screen.dart';
import 'package:wall_candy/views/widgets/CustomAppBar.dart';
import 'package:wall_candy/views/widgets/SearchBar.dart';
import 'package:wall_candy/views/widgets/catBlock.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/home';
  HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<PhotosModel> trendingWallpapers = [];

  GetTrendingWallpapers() async {
    trendingWallpapers = await ApiOperations.getTrendingWallpapers();
    setState(() {});
  }

  List<CategorieModel> categories = getCategories();

  @override
  void initState() {
    super.initState();
    GetTrendingWallpapers();
    ApiOperations.getTrendingWallpapers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor,
        title: GestureDetector(
          onTap: () {
            Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(
                  Icons.support,
                  color: Colors.black54,
                  size: 32,
                ),
                SizedBox(
                  width: 5,
                ),
                CustomAppBar(word1: 'Wall', word2: 'Candy'),
              ],
            ),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
        child: Column(
          children: [
            SearchBar(),
            const SizedBox(height: 20),
            SizedBox(
              height: 75,
              width: MediaQuery.of(context).size.width,
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (ctx, idx) => InkWell(onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (ctx)=> CategoryScreen(categoryTitle: categories[idx].categorieName, categoryUrl: categories[idx].imgUrl)));
                }, child: CatBlock(catImgSrc: categories[idx].imgUrl, category: categories[idx].categorieName)), 
                itemCount: categories.length,
              ),
            ),
            Expanded(
              child: SizedBox(
                // height: MediaQuery.of(context).size.height,
                child: GridView.builder(
                  physics: BouncingScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    mainAxisExtent: 300,
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                  ),
                  itemCount: trendingWallpapers.length,
                  itemBuilder: (ctx, idx) => InkWell(
                    splashColor: Colors.blueGrey,
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (ctx) => FullScreen(
                            imgUrl: trendingWallpapers[idx].imgSrc,
                            photographer: trendingWallpapers[idx].photographer,
                            desc: trendingWallpapers[idx].desc,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.grey,
                      ),
                      clipBehavior: Clip.hardEdge,
                      child: Hero(
                        tag: trendingWallpapers[idx].imgSrc,
                        child: Image.network(
                          trendingWallpapers[idx].imgSrc,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
