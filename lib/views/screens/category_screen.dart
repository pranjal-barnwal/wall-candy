import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wall_candy/views/widgets/CustomAppBar.dart';
import 'package:wall_candy/views/screens/full_screen.dart';

import '../../controller/apiOperation.dart';
import '../../models/photosModel.dart';

class CategoryScreen extends StatefulWidget {
  static const routeName = '/category';
  final categoryTitle;
  final categoryUrl;

  CategoryScreen({
    required this.categoryTitle,
    required this.categoryUrl,
  });

  @override
  State<CategoryScreen> createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  List<PhotosModel> searchWallpapers = [];

  GetSearchResults() async {
    searchWallpapers = await ApiOperations.getSearchedWallpapers(widget.categoryTitle);
    setState(() {});
  }
  
  @override
  void initState() {
    super.initState();
    GetSearchResults();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
          color: Colors.black54, // Change the color here
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor,
        title: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0, right: 55),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Icon(
                  Icons.support,
                  color: Colors.black54,
                  size: 32,
                ),
                SizedBox(
                  width: 5,
                ),
                CustomAppBar(word1: 'Wall', word2: 'Candy'),
              ],
            ),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(15, 15, 15, 0),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
              ),
              clipBehavior: Clip.hardEdge,
              child: Stack(
                children: [
                  Hero(
                    tag: widget.categoryUrl,
                    child: Image.network(
                      widget.categoryUrl,
                      height: 150,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Container(
                    color: Colors.black38,
                    height: 150,
                    child: Center(
                    child: Text(
                      widget.categoryTitle,
                      style: GoogleFonts.pacifico(
                        fontSize: 32,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  ),
                  
                ],
              ),
            ),
            const SizedBox(height: 20),
            Expanded(
              child: SizedBox(
                child: GridView.builder(
                  physics: BouncingScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    mainAxisExtent: 300,
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                  ),
                  itemCount: searchWallpapers.length,
                  itemBuilder: (ctx, idx) => InkWell(
                    splashColor: Colors.blueGrey,
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (ctx) => FullScreen(
                            imgUrl: searchWallpapers[idx].imgSrc,
                            photographer: searchWallpapers[idx].photographer,
                            desc: searchWallpapers[idx].desc,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.grey,
                      ),
                      clipBehavior: Clip.hardEdge,
                      child: Image.network(
                        searchWallpapers[idx].imgSrc,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
