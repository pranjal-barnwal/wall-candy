# **Wall Candy**

<div align="center">
    <img alt="Demo" src="./public/mockup.png" />
</div>

<div align="center">

[![https://github.com/pranjal-barnwal/wall-candy](https://forthebadge.com/images/badges/open-source.svg)](https://github.com/pranjal-barnwal/wall-candy) &nbsp;
<img alt="Demo" src="./public/icon.png" height=60 />

<a href="https://github.com/pranjal-barnwal/wall-candy"><img src="https://img.shields.io/badge/-v:_1.0-black?style=flat-square&logo=android&logoColor=5bb882&link=https://github.com/pranjal-barnwal/wall-candy" /></a>
<a href="https://github.com/pranjal-barnwal/wall-candy"><img src="https://img.shields.io/badge/-Flutter-black?style=flat-square&logo=flutter&logoColor=54c5f8&link=https://github.com/pranjal-barnwal/wall-candy" /></a>
<a href="https://github.com/pranjal-barnwal/wall-candy/blob/main/build/app/outputs/flutter-apk/app-release.apk"><img src="https://img.shields.io/badge/-Android_App-5bb882?style=flat-square&logo=android&logoColor=white&link=https://github.com/pranjal-barnwal/wall-candy/blob/main/build/app/outputs/flutter-apk/app-release.apk" /></a>
<a href="https://wall-candy.sourceforge.io"><img src="https://img.shields.io/badge/-Source_Forge-ff6600?style=flat-square&logo=firebase&logoColor=white&link=https://github.com/pranjal-barnwal/wall-candy/blob/main/build/app/outputs/flutter-apk/app-release.apk" /></a>

[![Download Wall Candy](https://a.fsdn.com/con/app/sf-download-button)](https://sourceforge.net/projects/wall-candy/files/com.pranjalsspace.wall_candy.apk/download)

![https://github.com/pranjal-barnwal/wall-candy](https://img.shields.io/github/stars/pranjal-barnwal/wall-candy?color=red&logo=github&style=for-the-badge) &nbsp;
![https://github.com/pranjal-barnwal/wall-candy](https://img.shields.io/github/forks/pranjal-barnwal/wall-candy?color=red&logo=github&style=for-the-badge)

</div>

<h3 align="center">
    🔹
    <a href="https://github.com/pranjal-barnwal/wall-candy/issues">Report Bug</a> &nbsp; &nbsp;
    🔹
    <a href="https://github.com/pranjal-barnwal/wall-candy/issues">Request Feature</a>
</h3>

<br>

## App Features
- Collection of 3 million+ photos
- Categories selection
- Dynamic trending collection page
- Search image
- Hero Animations
- Open-source & Ad-free

<br>

`Built with Pexels API
`

<img src='https://www.screnter.com/wp-content/uploads/2021/01/pexels-logo.jpg' alt='Pexels API' width=100>
<br>

<br>


## Libraries Used
- **Material Library:** Consists of commonly used Material Widgets
- **Google Fonts:** Collection of Free & Open-Source fonts
- **Image Downloader:** Downloads images and movies and saves to Photo Library

<br>

## License

    Copyright © Pranjal Kumar

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

